$(document).ready(function () {

	$("#cancel").click(function () {		
		$('#create2').hide();
		$('#create').show();
	});

	$("#boardName").keydown(function (event) {             // Submit on hit enter
		if (event.keyCode == 13) {
			$('#submit').click();
			return false;
		}
	});


	$("#submit").click(function (e) {                       //create new Board
		e.preventDefault();
		var board = $('#boardName').val(); 

		if (board == "") {
			$("#error").text('Enter board Name !');
			return false;
		} else {
			var data = { "board": board };
			$.ajax({
				type: 'POST',
				url: 'php-ajax/insert_board.php', 
				data: data,
				success: function (res) {
					$('#myForm')[0].reset();
					data = $.parseJSON(res);
					$("#boards").append(	
						$('<div/>')
							.attr("id", data.board_id)
							.addClass("board")
							.append(
								$('<a/>')
									.attr("href", "board.php?id=" +data.board_id)
									.append("<h4>" + data.board_name + "</h4>")
							)
					);
					$("#create2").hide();
					$("#create").show();
				}
			});
		}

	});


	$("#create").click(function () {                 //Open Board form
		$('#create').hide();
		$('#create2').show();
	});


	$("#addList").click(function (e) {            //Open List form
		$(this).hide();
		$('#addListForm').show();
		$('#labName').focus();
	});

	$(".close").click(function (e) {               //Close List form
		$('#addListForm').hide();
		$('#addList').show();
	});


	$("#labName").keydown(function (e) {            //Enter equal to Submit list form
		if (e.keyCode == 13) {
			$('#listForm').submit();
		}
	});

	$("#listForm").submit(function (e) {            // On submit list form
		e.preventDefault();

		var list = $("#labName").val();
		var boardID = $('#boardID').val();
		$('#listForm')[0].reset();

		if (list == "") {
			$('#listError').html('Enter List Name !');
			return false;
		} 
		else {
			var data = {'boardID': boardID, 'list': list};
			$.ajax({
				type: 'POST',
				url: 'php-ajax/insert_list.php',
				data: data,
				success: function (res) {
					$('#addListForm').hide();
					$('#addList').show();

					data = $.parseJSON(res);

					$("#lists").append(
						$('<div/>')
							.attr("id", data.list_id)
							.addClass("list")
							.append([
								$("<div/>")
									.addClass('listTitle')
									.append([
										$('<h3>'+ data.list_name +'</h3>'),
										$('<hr>')
									]),
								$("<div/>")
									.addClass('listItems')
									.append(
										$('<div/>')
											.addClass('listItem')
											.append(
												$("<form/>")
													.addClass('formListItem')
													.append(
														$('<input/>')
															.addClass('txtListItem')
															.attr({
																type: "text",
																placeholder: "Add Task"
															})
													)
											)
									)
							])
					);
				}
			});
		}
	});

	$(".txtListItem").on('keydown', function (e) {            //Enter equal to Submit list form
		if (e.keyCode == 13) {
			$(this).parent().submit();
			return false;
		}

	});


	// Submit new task

	$(".formListItem").on('submit', function (e) {
		e.preventDefault();
		var list_id = $(this).closest('div.list').attr('id');    // Get id of list
		var task 	= $(this).find('.txtListItem').val();        // Get name of textbox
		var sortedIDs = sortOrder(list_id); 

		if (task) {
			$(this).css('border', 'none');
			$(this)[0].reset();
			var listItems = $(this).parent().next('div.listItems');      //Get ListItem

			var data = { 'list_id': list_id, 'task': task, 'sortedIDs': sortedIDs };
			$.ajax({
				type: "POST",
				url: "php-ajax/insert_task.php",
				data: data,
				success: function (res) {

					data = $.parseJSON(res);
					listItems.append(
						$("<div/>")
							.addClass('listItem')
							.attr('id', data.task_id)
							.append('<h3>'+ data.task_name +'</h3>' )
					);
				}
			});
			
		} else {
			$(this).css('border', '1px solid red');
		}

	});

	function sortOrder(id) {
		var sortedIDs = $( "#"+id ).find('.listItems').sortable( "toArray" );
		return sortedIDs;
	}


	// Drag and drop tasks to lists
	$(function () {
		var drag_list_id, drop_list_id, drag_order, drop_position;
		var handle = $('.listItem').find('h3');

		$('.listItems').sortable({
			handle: handle,
			cancel: "input",
			connectWith: ".listItems",
			dropOnEmpty: true,
			cursor: "move",
			opacity: 0.8,
			start: function(event, ui) {
				drag_list_id = ui.item.closest('.list').attr('id');
	      	ui.item.startPos = ui.item.index();
	    	},
			stop: function(event, ui) {
			  drag_order = ui.item.startPos;
			  drop_position = ui.item.index();
			  drop_list_id = ui.item.closest('.list').attr('id');

			  if ( (drag_order != drop_position) || (drag_list_id != drop_list_id) ) {
			  		var list_order = [];
			  		// var drop_list_order = {list_id: drop_list_id, listItems:[]};

			  		$('#'+drag_list_id).find('.listItems .listItem').each(function (e) {
			  			list_order.push(
			  				{ list_id: drag_list_id ,task_id: $(this).attr('id'), order: $(this).index() }
			  			);
			  		});

			  		$('#'+drop_list_id).find('.listItems .listItem').each(function (e) {
			  			list_order.push(
			  				{ list_id: drop_list_id, task_id: $(this).attr('id'), order: $(this).index() }
			  			);
			  		});
			  		console.log(list_order);
			  		var data = { 'list_order': list_order };
			  		$.ajax({
			  			type 	: 'POST',
			  			url 	: 'php-ajax/update_order.php',
			  			data	: JSON.stringify(data),
			  			success : function (res) {
			  				alert(res);
			  			}
			  		});
			  } 

			}
		}).disableSelection();

	});
	
});