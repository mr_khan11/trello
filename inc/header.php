<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title> <?= $title ?> </title>
	<link rel="stylesheet" href="css/bootstrap.min.css">    <!--  bootstrap css -->
	<link rel="stylesheet" href="css/style.css">
</head>
<body>

	<header id="header">  
		<div class="container">
			<a href="">
				<img src="icon.png" width="90px" alt="Icon">
			</a>
		</div>
	</header>