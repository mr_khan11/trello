<?php  
	require 'inc/db.php';
	$id;
	if (isset($_GET['id'])) {
		$id = $_GET['id'];

		$sql = " SELECT * FROM boards WHERE id = '$id' ";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				$board = $row['name'];
			}
		}
	}

	$title = $board;
	include 'inc/header.php'; 
?>

	<div id="Wrapper"  class="container">
		<div id="titleWrapper" class="row">
			<div class="col-md-2" id="boardTitle">
				<h2> <?= $board ?> </h2>		
			</div>
			<div id="addList" class="col-md-1">
				<h4> Add a list.. </h4>
			</div>
			<div id="addListForm" class="col-md-2">
				<div class="close">
					X
				</div>
				<form id="listForm">
					<label for="labName"> Add List </label>
					<input type="text" id="labName" placeholder="Enter List Name">
					<input type="hidden" id="boardID" value="<?= $id ?>">
					<div id="listError"></div>
				</form>
			</div>
		</div>

		<div id="lists">
			<?php  
				$sql = " SELECT * FROM lists WHERE board_id = '$id' ";
				$result = $conn->query($sql);
				if ($result->num_rows > 0) {
					while ($row = $result->fetch_assoc()) {
			?>
						<div class="list" id="<?= $row['id'] ?>">
							<div class="listTitle">
								<h3> <?= $row['name'] ?> </h3>
								<hr>
							</div>
							
							<div class="listItem">
								<form class="formListItem">
									<input type="text" class="txtListItem" placeholder="Add Task ">
								</form>
							</div>

							<div class="listItems">
								<?php 
									$sql1 = " SELECT * FROM tasks WHERE list_id = '" . $row['id'] . "' ORDER BY sort ASC ";
									$result1 = $conn->query($sql1);
									if ($result1->num_rows > 0) {
										while ($row1 = $result1->fetch_assoc()) {
											
								?>
								<div class="listItem" id="<?= $row1['id'] ?>">
									<h3> <?= $row1['name'] ?> </h3>
									<!-- <span class="check"> &#x2713; </span> -->
								</div>
								
								<?php  
										}
									}
								?>
							</div>
						</div>
			<?php
					}
				}
			?>
			
		</div>	
	</div>

<?php include 'inc/footer.php'; ?>