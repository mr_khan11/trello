	
	<?php 
		$title = "Trello Practise";
		include 'inc/header.php'; 
	?>

	<section id="boards" class="container">
		<!-- <main class="container"> -->
			<div id="create" class="board">
				<h4> Create new board.. </h4>
			</div>
			<div id="create2" class="board">
				<h4> Create new board.. </h4>
				<div id="createForm">
					<form method="POST" id="myForm">
						<label for="boardName"> What shall we call the board ? </label>
						<input type="text" id="boardName" name="boardName"> <div id="error"></div>
						<button id="cancel" onclick="return false;"> CANCEL </button> 
						<button type="submit" id="submit"> CREATE </button>
					</form>
				</div>
			</div>
			<?php 
				require 'inc/db.php';
				$sql = " SELECT * FROM boards ";
				$result = $conn->query($sql);

				if ($result->num_rows > 0) {
					while ($row = $result->fetch_assoc()) {
			?>
				<div class="board" id="<?= $row['id'] ?>">
					<a href="board.php?id=<?= $row['id'] ?>">
						<h4> <?= $row['name'] ?> </h4>
					</a>
				</div>
			<?php 
					}
				}
			?>
		<!-- </main> -->
	</section>
	
	<?php include 'inc/footer.php'; ?>